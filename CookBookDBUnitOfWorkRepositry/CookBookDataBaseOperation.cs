﻿using CookBook.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;

namespace CookBookDBUnitOfWorkRepositry
{

    public class CookBookDataBaseOperation
    {
        public int SaveRecipe(Recipe lst)
        {
            // int storeID = Convert.ToInt32(cmdStore.SelectedValue.ToString());
            SqlConnection sConn = new SqlConnection();
            SqlParameter[] sParam = new SqlParameter[1];
            sConn.ConnectionString = "Server=tcp:demodbcb.database.windows.net,1433;Initial Catalog=CookBook;Persist Security Info=False;" +
                "User ID=demodbcb;Password={your_password};MultipleActiveResultSets=False;Encrypt=True;TrustServerCertificate=False;Connection Timeout=30;";

            //SqlConnection con = new SqlConnection(sConn.ConnectionString);
            sConn.Open();
            SqlCommand cmd = new SqlCommand("insert into Recipe  (RecipeCategoryType,Summary," +
                "SampleImagePath, Ingredients,CreatedBy, CreatedDateTime, IsActive) values " +
                "('Test','yyt', 'dd','dddd','dddwfd'," + DateTime.Now.ToString()  + ",1)", sConn);
            cmd.CommandType = CommandType.Text;
            cmd.Connection = sConn;

            int k = cmd.ExecuteNonQuery();
            return k;

        }

        public List<Recipe> GetAll()
        {
            List<Recipe> recipeData = new List<Recipe>();
            
            // int storeID = Convert.ToInt32(cmdStore.SelectedValue.ToString());
            SqlConnection sConn = new SqlConnection();
            SqlParameter[] sParam = new SqlParameter[1];
            sConn.ConnectionString = "Server=tcp:demodbcb.database.windows.net,1433;Initial Catalog=CookBook;Persist Security Info=False;" +
                "User ID=demodbcb;Password={your_password};MultipleActiveResultSets=False;Encrypt=True;TrustServerCertificate=False;Connection Timeout=30;";

            //SqlConnection con = new SqlConnection(sConn.ConnectionString);
            //con.Open();
            SqlCommand cmd = new SqlCommand("select * from Recipe", sConn);
            cmd.CommandType = CommandType.Text;
            cmd.Connection = sConn;
            sConn.Open();
            SqlDataReader dr = cmd.ExecuteReader();
            
            while (dr.Read())
            {
                Recipe rec = new Recipe();
                rec.description = dr["description"].ToString();
                rec.name = dr["name"].ToString();
                rec.imagepath = dr["recipeData"].ToString();
                //rec.ingredients = dr["ingredients"].ToString();
                rec.recipeCategory = dr["recipeCategory"].ToString();
                rec.IsActive = dr["isactive"].ToString();
                recipeData.Add(rec);
            }
            return recipeData;

        }
    }

}