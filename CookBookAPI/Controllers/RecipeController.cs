﻿using CookBook.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;

namespace CookBookAPI.Controllers
{
    [ApiController]
    [Route("api/[controller]/[Action]")]
    public class RecipeController : ControllerBase
    {
        private static readonly string[] Summaries = new[]
        {
            "Freezing", "Bracing", "Chilly", "Cool", "Mild", "Warm", "Balmy", "Hot", "Sweltering", "Scorching"
        };

        private readonly ILogger<RecipeController> _logger;

        public RecipeController(ILogger<RecipeController> logger)
        {
            _logger = logger;
        }
        [HttpPost]
        public int LoadRecipe(string recipe)
        {
            var jsonString = recipe.ToString();
            //CookBookDBUnitOfWorkRepositry.CookBookDataBaseOperation CRUDOperation = new CookBookDBUnitOfWorkRepositry.CookBookDataBaseOperation();
            ////Recipe recipe = (Recipe)recipe;
            ////JsonObject jsonObject = recipe.getAsJsonObject();

            //Recipe dyn = JsonConvert.DeserializeObject<Recipe>(recipe);
            //int recordInserted = CRUDOperation.SaveRecipe(dyn);
            return 1;


        }




        [HttpGet]
        public int CreateRecipe(string recipe)
        {
            var jsonString = recipe.ToString();
            CookBookDBUnitOfWorkRepositry.CookBookDataBaseOperation CRUDOperation =
                new CookBookDBUnitOfWorkRepositry.CookBookDataBaseOperation();

            Recipe recipeData = JsonConvert.DeserializeObject<Recipe>(recipe);
            int recordInserted = CRUDOperation.SaveRecipe(recipeData);
            return 1;


        }

        [HttpGet]
        public List<Recipe> GetAllRecipe()
        {
            //var jsonString = recipe.ToString();
            CookBookDBUnitOfWorkRepositry.CookBookDataBaseOperation CRUDOperation =
                new CookBookDBUnitOfWorkRepositry.CookBookDataBaseOperation();
            //Recipe thePerson = (Recipe)recipe;
            //JsonObject jsonObject = recipe.getAsJsonObject();
            List<Recipe> recipeData = CRUDOperation.GetAll();
            return recipeData;
            //Recipe recipeData = JsonConvert.DeserializeObject<Recipe>(recipe);
            //int recordInserted = CRUDOperation.SaveRecipe(recipeData);
            //return 1;


        }




    }
}
