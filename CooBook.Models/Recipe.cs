﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace CookBook.Models
{
    public class Recipe
    {

        public int RecipeID { get; set; }
        public string name { get; set; }
        [JsonProperty("description")]
        public string description { get; set; }
        [JsonProperty("imagepath")]
        public string imagepath { get; set; }
        public List<string> ingredients { get; set; }
        [JsonProperty("recipeCategory")]
        public string recipeCategory
        { get; set; }
        public string CreatedDateTime
        { get; set; }
        public string IsActive
        { get; set; }
    }
}